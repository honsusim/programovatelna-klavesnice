from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from os import *
from pynput import keyboard
import serial.tools.list_ports as listPorts
from serial_threads import *
import sys
import os

# get resource path eso that only one executible file can be craeted
def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

# main app window class
class Window(QMainWindow):
    def __init__(self):
        super().__init__()

        # window setup
        self.setWindowTitle("Macropad configuration")
        self.setWindowIcon(QIcon(resource_path("assets/icon.ico")))
        # self.setFixedSize(800, 650)

        # create settings in the right path for launch at startup
        self.startupSettings = QSettings("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Run", QSettings.NativeFormat) 

        self.createActions()
        self.createMenubar()
        self.createToolbar()
        self.createTabWidget()
        self.createTrayIcon()
        self.connectActions()

        self.activeProfile = 0

        self.readSettings()
        self.createSerialThreads()

    def createActions(self):
        self.addAction = QAction(QIcon(resource_path("assets/add.svg")), "&Add profile", self)
        self.deleteAction = QAction(QIcon(resource_path("assets/delete.svg")), "&Delete profile", self)
        self.exitAction = QAction(QIcon(resource_path("assets/exit.svg")), "&Exit", self)
        self.renameAction = QAction(QIcon(resource_path("assets/rename.svg")), "&Rename profile", self)
        self.saveAction = QAction(QIcon(resource_path("assets/save.svg")), "&Save", self)
        self.uploadAction = QAction(QIcon(resource_path("assets/upload.svg")), "&Upload", self)

    def connectActions(self):
        self.addAction.triggered.connect(self.addProfile)
        self.deleteAction.triggered.connect(self.deleteProfile)
        self.renameAction.triggered.connect(self.renameProfile)
        self.exitAction.triggered.connect(self.close)
        self.saveAction.triggered.connect(self.writeSettings)
        self.uploadAction.triggered.connect(self.uploadSettings)
    
    def createMenubar(self):
        menubar = self.menuBar()

        fileMenu = QMenu("&File", self)
        fileMenu.addAction(self.saveAction)
        fileMenu.addAction(self.uploadAction)
        fileMenu.addAction(self.exitAction)

        editMenu = QMenu("&Edit", self)
        editMenu.addAction(self.addAction)
        editMenu.addAction(self.deleteAction)
        editMenu.addAction(self.renameAction)

        menubar.addMenu(fileMenu)
        menubar.addMenu(editMenu)
    
    def createToolbar(self):
        # create the toolbar
        toolbar = self.addToolBar("Toolbar")
        toolbar.setMovable(False)
        self.portLabel = QLabel("Port:")
        self.portLabel.setMargin(5)
        self.port = QLabel("None")
        self.port.setMargin(5)
        self.portStatusLabel = QLabel("Device status:")
        self.portStatusLabel.setMargin(5)
        self.portStatus = QLabel("Disconnected")
        self.portStatus.setMargin(5)
        self.startupCheck = QCheckBox("Launch at startup")
        self.startupCheck.setChecked(self.startupSettings.contains("Macropad")) # set checked if there already is a registry record for launch at startup
        self.startupCheck.stateChanged.connect(self.launchAtStartupHandle)

        toolbar.addAction(self.addAction)
        toolbar.addAction(self.deleteAction)
        toolbar.addAction(self.renameAction)
        toolbar.addAction(self.saveAction)
        toolbar.addAction(self.uploadAction)
        toolbar.addSeparator()
        toolbar.addWidget(self.startupCheck)
        toolbar.addSeparator()
        toolbar.addWidget(self.portLabel)
        toolbar.addWidget(self.port)
        toolbar.addSeparator()
        toolbar.addWidget(self.portStatusLabel)
        toolbar.addWidget(self.portStatus)

    def createTabWidget(self):
        self.tabWidget = QTabWidget()
        self.setCentralWidget(self.tabWidget)
        self.tabWidget.setMovable(True)

        # create corner profile label
        label = QLabel("Profiles:")
        label.setMargin(5)
        label.setIndent(3)
        self.tabWidget.setCornerWidget(label, Qt.TopLeftCorner)

    def createTrayIcon(self):
        self.trayIcon = QSystemTrayIcon()
        self.trayIcon.setIcon(QIcon(resource_path("assets/icon.ico")))
        self.trayIcon.setToolTip("Macropad configuration")
        self.trayIcon.show()
        self.trayIcon.activated.connect(self.show)

    def addProfile(self):
        # dialog setup
        dialog = QDialog(self)
        dialog.setWindowIcon(QIcon(resource_path("assets/add.svg")))
        dialog.setWindowTitle("Add profile")
        
        # create widgets
        message = QLabel("Enter the profile name:")
        lineEdit = QLineEdit()
        lineEdit.setMaxLength(12)        
        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        
        # connect buttons
        buttonBox.accepted.connect(dialog.accept)
        buttonBox.rejected.connect(dialog.reject)
        
        # layout setup
        layout = QVBoxLayout()
        layout.addWidget(message)
        layout.addWidget(lineEdit)
        layout.addWidget(buttonBox)
        dialog.setLayout(layout)
        
        if self.tabWidget.count() < 10:
            if dialog.exec():
                self.tabWidget.addTab(profileWidget(), lineEdit.text())  
        else:
            msgBox = QMessageBox(self)
            msgBox.setText("Can't have more than 10 profiles.")
            msgBox.setWindowIcon(QIcon(resource_path("assets/error.svg")))
            msgBox.setIcon(QMessageBox.NoIcon)
            msgBox.setWindowTitle("Add profile error")
            msgBox.exec()

    def deleteProfile(self):
        # dialog setup
        dialog = QDialog(self)
        dialog.setWindowIcon(QIcon(resource_path("assets/delete.svg")))
        dialog.setWindowTitle("Delete profile")

        # create widgets
        message = QLabel("Select a profile to delete:")
        comboBox = QComboBox() 
        for i in range(self.tabWidget.count()): # populates the combobox with tab names in the right order
            comboBox.addItem(self.tabWidget.tabText(i))
        comboBox.setCurrentIndex(self.tabWidget.currentIndex()) # sets the combobox to currently viewed tab
        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        
        # connect buttons
        buttonBox.accepted.connect(dialog.accept)
        buttonBox.rejected.connect(dialog.reject)
        
        # layout setup
        layout = QVBoxLayout()
        layout.addWidget(message)
        layout.addWidget(comboBox)
        layout.addWidget(buttonBox)
        dialog.setLayout(layout)
        
        if dialog.exec():
            # searches for the selected profile tab
            for i in range(self.tabWidget.count()):
                if self.tabWidget.tabText(i) == comboBox.currentText():
                    self.tabWidget.removeTab(i) # removes the selected tab
                    break

    def renameProfile(self):
        # dialog setup
        dialog = QDialog(self)
        dialog.setWindowIcon(QIcon(resource_path("assets/rename.svg")))
        dialog.setWindowTitle("Rename profile")

        # create widgets
        message = QLabel("Select a profile to rename:")
        comboBox = QComboBox() 
        for i in range(self.tabWidget.count()): # populates the combobox with tab names in the right order
            comboBox.addItem(self.tabWidget.tabText(i))
        comboBox.setCurrentIndex(self.tabWidget.currentIndex()) # sets the combobox to currently viewed tab
        message2 = QLabel("New name:")
        lineEdit = QLineEdit()
        lineEdit.setMaxLength(12) 
        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        
        # connect buttons
        buttonBox.accepted.connect(dialog.accept)
        buttonBox.rejected.connect(dialog.reject)
        
        # layout setup
        layout = QVBoxLayout()
        layout.addWidget(message)
        layout.addWidget(comboBox)
        layout.addWidget(message2)
        layout.addWidget(lineEdit)
        layout.addWidget(buttonBox)
        dialog.setLayout(layout)

        if dialog.exec():
            # searches for the selected profile tab
            for i in range(self.tabWidget.count()):
                if self.tabWidget.tabText(i) == comboBox.currentText():
                    self.tabWidget.setTabText(i, lineEdit.text()) # renames the selected tab
                    break

    def closeEvent(self, event):

        # add Save/Discard/Cancel dialog with buttonBox

        if event.spontaneous() == True:
            # if the 'X' button in the corner is clicked (event is spontaneous), the app is hidden
            self.writeSettings()
            event.ignore()
            self.hide()
        else:
            # if the exit action is called (event isn't spontaneous), the app is closed
            self.trayIcon.hide()
            self.writeSettings()
            event.accept()

    def launchAtStartupHandle(self):
        # if the launch at startup is active, save the program path to registry, otherwise remove the registry record
        if self.startupCheck.isChecked():
            self.startupSettings.setValue("Macropad", sys.argv[0])
        else:
            self.startupSettings.remove("Macropad")
            
    def writeSettings(self):
        settings = QSettings("honsusim", "Macropad configuration")

        # save profiles
        settings.beginWriteArray("profiles")
        for profile in range(self.tabWidget.count()):
            settings.setArrayIndex(profile) # set the profile index
            settings.setValue("profileName", self.tabWidget.tabText(profile)) # save the profile name
            
            # save keys
            for key in range (12):
                settings.setValue(f"keyFunction{key}", self.getKey(profile, key).functionList.currentText())    # key function
                settings.setValue(f"keyName{key}", self.getKey(profile, key).keyName.text())                    # key name
                settings.setValue(f"mediaFunction{key}", self.getKey(profile, key).mediaFunction.currentText()) # key media function
                settings.setValue(f"shortcut{key}", self.getKey(profile, key).shortcut)                         # key shortcut list
                settings.setValue(f"shortcutText{key}", self.getKey(profile, key).shortcutLabel.text())         # key shortcut text
                settings.setValue(f"typeText{key}", self.getKey(profile, key).textInput.text())                 # key type text
                settings.setValue(f"scriptLocation{key}", self.getKey(profile, key).scriptLocation)             # key script location
                settings.setValue(f"scriptName{key}", self.getKey(profile, key).scriptLabel.text())             # key script name

            # save encoders
            for enc in range(2):
                settings.setValue(f"cwFunction{enc}", self.getEncoder(profile, enc).cwFunction.currentText())   # encoder cw function
                settings.setValue(f"ccwFunction{enc}", self.getEncoder(profile, enc).ccwFunction.currentText()) # encoder ccw function
        settings.endArray()

        settings.setValue("activeProfile", self.activeProfile) # save active profile

    def readSettings(self):
        settings = QSettings("honsusim", "Macropad configuration")

        # restore profiles
        size = settings.beginReadArray("profiles")
        for profile in range(size):
            settings.setArrayIndex(profile)
            self.tabWidget.addTab(profileWidget(), settings.value("profileName")) # add a profile tab with the saved name

            # restore keys
            for key in range(12):
                self.getKey(profile, key).functionList.setCurrentText(settings.value(f"keyFunction{key}"))      # key function
                self.getKey(profile, key).keyName.setText(settings.value(f"keyName{key}"))                      # key name
                self.getKey(profile, key).mediaFunction.setCurrentText(settings.value(f"mediaFunction{key}"))   # key media function
                self.getKey(profile, key).shortcut = settings.value(f"shortcut{key}")                           # key shortcut list
                self.getKey(profile, key).shortcutLabel.setText(settings.value(f"shortcutText{key}"))           # key shortcut text
                self.getKey(profile, key).textInput.setText(settings.value(f"typeText{key}"))                   # key type text
                self.getKey(profile, key).scriptLocation = settings.value(f"scriptLocation{key}")               # key script location
                self.getKey(profile, key).scriptLabel.setText(settings.value(f"scriptName{key}"))               # key script name
            
            # restore encoders
            for enc in range(2):
                self.getEncoder(profile, enc).cwFunction.setCurrentText(settings.value(f"cwFunction{enc}"))     # encoder cw function
                self.getEncoder(profile, enc).ccwFunction.setCurrentText(settings.value(f"ccwFunction{enc}"))   # encoder ccw function
        settings.endArray()

        # read saved active profile if there is any, otherwise set the active profile to 0
        if settings.contains("activeProfile"):
            self.activeProfile = settings.value("activeProfile")
        else:
            self.activeProfile = 0

    def getKey(self, profile: int, key: int):
        # returns the specified key from the specified profile
        return self.tabWidget.widget(profile).keyList[key]
    
    def getEncoder(self, profile: int, encoder: int):
        # returns the specified encoder from the specified profile
        return self.tabWidget.widget(profile).encoderList[encoder]

    def createSerialThreads(self):
        self.deviceConnected = False
        self.uploading = False

        self.handlerThread = QThread()
        self.portHandler = PortHandler(self)
        self.portHandler.moveToThread(self.handlerThread)
        self.handlerThread.started.connect(self.portHandler.run)

        self.finderThread = QThread()
        self.portFinder = PortFinder(self)
        self.portFinder.moveToThread(self.finderThread)
        self.finderThread.started.connect(self.portFinder.run)

        self.portHandler.deviceRemoved.connect(self.startPortFind)
        self.portFinder.deviceConnected.connect(self.startPortHandle)

        self.finderThread.start()

    def startPortFind(self):
        self.handlerThread.quit()
        time.sleep(0.1)
        self.finderThread.start()

    def startPortHandle(self):
        self.finderThread.quit()
        time.sleep(0.1)
        self.handlerThread.start()
    
    def uploadSettings(self):
        if self.deviceConnected == False:
            msgBox = QMessageBox(self)
            msgBox.setText("Connect the device before uploading.")
            msgBox.setWindowIcon(QIcon(resource_path("assets/error.svg")))
            msgBox.setIcon(QMessageBox.NoIcon)
            msgBox.setWindowTitle("Upload error")
            msgBox.exec()
            return
        
        if self.tabWidget.count() == 0:
            msgBox = QMessageBox(self)
            msgBox.setText("Add at least one profile before uploading.")
            msgBox.setWindowIcon(QIcon(resource_path("assets/error.svg")))
            msgBox.setIcon(QMessageBox.NoIcon)
            msgBox.setWindowTitle("Upload error")
            msgBox.exec()
            return 
        
        self.uploading = True
        self.portHandler.serialPort.close()


class profileWidget(QWidget):
    def __init__(self):
        super().__init__()

        # create a list of all 12 keyWidgets for easy access
        self.keyList = []
        for i in range(12):
            self.keyList.append(keyWidget())

        # creates a 3x4 grid layout and populates it with keyWidgets
        layout = QGridLayout()
        self.setLayout(layout)
        for row in range(4):
            for col in range(3):
                layout.addWidget(self.keyList[col + 3*row], row, col)
                self.keyList[col + 3*row].setTitle(f"KEY {col + 3*row + 1}")

        # create a list of encoders and add them to layout
        self.encoderList = []
        for i in range(2):
            self.encoderList.append(encoderWidget())
            layout.addWidget(self.encoderList[i], i, 3)
            self.encoderList[i].setTitle(f"ENCODER {i + 1}")
        self.encoderList[0].clickFunction.setText("Previous profile")
        self.encoderList[1].clickFunction.setText("Next profile")


class encoderWidget(QGroupBox):
    def __init__(self):
        super().__init__()

        # widgets setup
        self.clickFunction = QLabel()
        self.cwFunction = QComboBox()
        self.cwFunction.addItems(["None", "Volume +", "Volume -", "Scroll up", "Scroll down", "Scroll left", "Scroll right"])
        self.ccwFunction = QComboBox()
        self.ccwFunction.addItems(["None", "Volume +", "Volume -", "Scroll up", "Scroll down", "Scroll left", "Scroll right"])

        # layout setup
        layout = QGridLayout()
        self.setLayout(layout)
        layout.setAlignment(Qt.AlignTop | Qt.AlignLeft)
        layout.addWidget(QLabel("Click: "), 0, 0)
        layout.addWidget(self.clickFunction, 0, 1)
        layout.addWidget(QLabel("Rotate CW: "), 1, 0)
        layout.addWidget(QLabel("Rotate CCW: "), 2, 0)
        layout.addWidget(self.cwFunction, 1, 1)
        layout.addWidget(self.ccwFunction, 2, 1)


class keyWidget(QGroupBox):
    def __init__(self):
        super().__init__()

        # widgets setup
        self.keyName = QLineEdit()
        self.keyName.setMaxLength(6)
        self.functionList = QComboBox()
        self.functionList.addItems(["None", "Media", "Shortcut", "Type", "Script"])
        self.createSettings() # creates the functionSettings groupbox    

        # layout setup
        layout = QGridLayout()
        self.setLayout(layout)
        layout.addWidget(QLabel("Name:"), 0, 0)
        layout.addWidget(self.keyName, 0, 1)
        layout.addWidget(QLabel("Function:"), 0, 2)
        layout.addWidget(self.functionList, 0, 3)
        layout.addWidget(self.functionSettings, 1, 0, 3, 4)

        # connect function list combobox
        self.functionList.currentTextChanged.connect(self.selectFunction) 

    def createSettings(self):
        # setup groupbox and stacked layout
        self.functionSettings = QGroupBox()
        self.functionSettings.setTitle("Function settings")        
        self.stackedLayout = QStackedLayout()
        self.functionSettings.setLayout(self.stackedLayout)

        # add widgets to the stacked layout
        self.createNoneSettings()       # self.noneSettings  
        self.createMediaSettings()      # self.mediaSettings
        self.createShortcutSettings()   # self.shortcutSettings
        self.createTypeSettings()       # self.typeSettings
        self.createScriptSettings()     # self.scriptSettings
    
    def selectFunction(self):
        function = self.functionList.currentText()
        if function == "None":
            self.stackedLayout.setCurrentWidget(self.noneSettings)
        elif function == "Media":
            self.stackedLayout.setCurrentWidget(self.mediaSettings)
        elif function == "Type":
            self.stackedLayout.setCurrentWidget(self.typeSettings)
        elif function == "Shortcut":
            self.stackedLayout.setCurrentWidget(self.shortcutSettings)
        elif function == "Script":
            self.stackedLayout.setCurrentWidget(self.scriptSettings)

    def createNoneSettings(self):
        self.noneSettings = QWidget()
        layout = QGridLayout()
        layout.setAlignment(Qt.AlignTop)
        self.noneSettings.setLayout(layout)

        self.stackedLayout.addWidget(self.noneSettings)

    def createMediaSettings(self):        
        self.mediaFunction = QComboBox()
        self.mediaFunction.addItems(["Play/Pause", "Next", "Previous", "Mute/Unmute", "Volume +", "Volume -"])

        self.mediaSettings = QWidget()
        layout = QGridLayout()
        layout.setAlignment(Qt.AlignTop | Qt.AlignLeft)
        self.mediaSettings.setLayout(layout)

        layout.addWidget(QLabel("Key function:"), 0, 0)
        layout.addWidget(self.mediaFunction, 0, 1)

        self.stackedLayout.addWidget(self.mediaSettings)

    def createShortcutSettings(self):
        # widgets setup
        self.recordButton = QPushButton("Start recording")
        removeButton = QPushButton("Remove shortcut")
        self.shortcutLabel = QLabel("Shortcut not set")
        self.shortcutLabel.setIndent(2)
        self.recordButton.clicked.connect(self.startRecording)
        removeButton.clicked.connect(self.removeShortcut)

        # layout setup
        self.shortcutSettings = QWidget()
        layout = QGridLayout()
        layout.setAlignment(Qt.AlignTop | Qt.AlignLeft)
        self.shortcutSettings.setLayout(layout)

        # add widgets to the layout
        layout.addWidget(self.recordButton, 0, 0)
        layout.addWidget(removeButton, 0, 1)
        layout.addWidget(self.shortcutLabel, 1, 0, 1, 2)

        # record helper variables
        self.keysPressed = set()
        self.shortcut = []
        self.shortcutText = []

        self.stackedLayout.addWidget(self.shortcutSettings)

    def startRecording(self):
        self.shortcut.clear()
        self.shortcutText.clear()
        self.recordButton.setText("Recording...")        
        self.listener = keyboard.Listener(on_press=self.onPress, on_release=self.onRelease, suppress=True)
        self.listener.start()

    def onPress(self, key):
        # convert the pressed key to string
        try:
            keyChar = key.char
        except AttributeError:
            keyChar = str(key)

        # add the key to the list of currently pressed keys
        self.keysPressed.add(keyChar)

        # if the key isn't already in the shortcut, add the key to the shortcut and show formatted shortcut
        if (not self.shortcut) or (not keyChar in self.shortcut):
            self.shortcut.append(keyChar)
            self.shortcutText.append(keyChar.replace("Key.", "").replace("_", " ").upper())
            self.shortcutLabel.setText(" + ".join(self.shortcutText))

    def onRelease(self, key):
        # convert the pressed key to string
        try:
            keyChar = key.char
        except AttributeError:
            keyChar = str(key)

        # remove the key from currently pressed keys
        self.keysPressed.remove(keyChar)

        # if any keys aren't pressed, stop recording
        if not self.keysPressed:
            self.recordButton.setText("Start recording")
            self.listener.stop()
            # print(self.shortcut)

    def removeShortcut(self):
        self.shortcut.clear()
        self.shortcutText.clear()
        self.shortcutLabel.setText("Shortcut not set")
        # print(self.shortcut)

    def createTypeSettings(self):
        self.textInput = QLineEdit()

        self.typeSettings = QWidget()
        layout = QGridLayout()
        layout.setAlignment(Qt.AlignTop | Qt.AlignLeft)
        self.typeSettings.setLayout(layout)

        layout.addWidget(QLabel("Text to type:"), 0, 0)
        layout.addWidget(self.textInput, 1, 0)

        self.stackedLayout.addWidget(self.typeSettings)

    def createScriptSettings(self):
        # widgets setup
        self.scriptButton = QPushButton("Select script")
        removeButton = QPushButton("Remove script")
        self.scriptLabel = QLabel("Script not set")
        self.scriptLabel.setIndent(2)
        self.scriptButton.clicked.connect(self.selectScript)
        removeButton.clicked.connect(self.removeScript)

        # layout setup
        self.scriptSettings = QWidget()
        layout = QGridLayout()
        layout.setAlignment(Qt.AlignTop | Qt.AlignLeft)
        self.scriptSettings.setLayout(layout)

        # add widgets to the layout
        layout.addWidget(self.scriptButton, 0, 0)
        layout.addWidget(removeButton, 0, 1)
        layout.addWidget(self.scriptLabel, 1, 0, 1, 2)

        self.scriptLocation = ""

        self.stackedLayout.addWidget(self.scriptSettings)

    def selectScript(self):
        self.scriptLocation = QFileDialog().getOpenFileName(self, "Select a python script", getcwd(), "Python files (*.py)") # returns the file location and used filters
        self.scriptLocation = self.scriptLocation[0] # stores only the file location
        self.scriptName = self.scriptLocation.rsplit("/", 1)[-1] # gets the file name from the file location
        if self.scriptLocation:
            self.scriptLabel.setText(self.scriptName) # if a script was selected, show its name;
        else:
            self.scriptLabel.setText("Script not set")

    def removeScript(self):
        self.scriptLocation = ""
        self.scriptName = ""
        self.scriptLabel.setText("Script not set")
