from PyQt5.QtCore import *
from pynput.keyboard import Key, Controller
from pynput.keyboard import Key, Controller as KeyboardController
from pynput.mouse import Controller as MouseController
import serial
import time
import serial.tools.list_ports as listPorts
import runpy

class PortHandler(QObject):
    
    deviceRemoved = pyqtSignal()

    def __init__(self, main_window):
        super().__init__()

        self.mainWindow = main_window           # reference to main app window
        self.keyboard = KeyboardController()    # controller for keyboard simulation 
        self.mouse = MouseController()          # controller for mouse simulation
        
        self.serialPort = serial.Serial()
        self.serialPort.timeout = None
        self.serialPort.baudrate = 115200

    def run(self):
        # try to open the serial port
        try:
            self.serialPort.port = self.mainWindow.port.text()
            self.serialPort.open()
            self.mainWindow.portStatus.setText("Connected") 
            self.mainWindow.deviceConnected = True           
        except Exception as e:
            # print(e)
            self.serialPort.close()
            self.mainWindow.port.setText("None")
            self.mainWindow.portStatus.setText("Disconnected")
            self.mainWindow.deviceConnected = False
            self.deviceRemoved.emit()
            return
        
        if self.mainWindow.uploading == True:            
            self.uploadSettings()            

        # try reading from serial port
        # if error -> disconnect -> start port finder
        while True:
            try:
                data = self.serialPort.read(3).decode()
                if not data == "":
                    print(data)
                    self.handleInput(data)  # handle keyboard input
            except Exception as e:
                # print(e)
                if self.mainWindow.uploading == False:                    
                    self.mainWindow.port.setText("None")
                    self.mainWindow.portStatus.setText("Disconnected")
                self.serialPort.close()
                self.mainWindow.deviceConnected = False
                self.deviceRemoved.emit()
                break
    
    def handleInput(self, data: str):
        profile = self.mainWindow.activeProfile
        profileCount = self.mainWindow.tabWidget.count()
        if profileCount == 0:
            return

        # handle keys input
        if data.startswith("K"):
            # get key function
            key = int(data[1:3])
            function = self.mainWindow.getKey(profile, key).functionList.currentText()

            # execute function
            if function == "None":
                return
            
            elif function == "Media":
                mediaFunction = self.mainWindow.getKey(profile, key).mediaFunction.currentText()
                if mediaFunction == "Play/Pause":
                    self.keyboard.tap(Key.media_play_pause)
                elif mediaFunction == "Next":
                    self.keyboard.tap(Key.media_next)
                elif mediaFunction == "Previous":
                    self.keyboard.tap(Key.media_previous)
                elif mediaFunction == "Mute/Unmute":
                    self.keyboard.tap(Key.media_volume_mute)
                elif mediaFunction == "Volume +":
                    self.keyboard.tap(Key.media_volume_up)
                elif mediaFunction == "Volume -":
                    self.keyboard.tap(Key.media_volume_down)

            elif function == "Type":
                text = self.mainWindow.getKey(profile, key).textInput.text()
                self.keyboard.type(text)

            elif function == "Shortcut":
                shortcut = self.mainWindow.getKey(profile, key).shortcut
                keyList = []

                # create list of shortcut key variables
                for i in range(len(shortcut)):
                    # if the key is a modifier, turn the string into variable, append variable
                    if shortcut[i].startswith("Key."):
                        mod, var = shortcut[i].split(".")
                        key = globals()[mod][var]   # get the variable using module's and variable's name
                        keyList.append(key)
                    # if the key is a char, apend char
                    else:
                        keyList.append(shortcut[i])

                # press all the keys in order
                for i in range(len(keyList)):
                    self.keyboard.press(keyList[i])

                # release all the keys in the opposite order
                for i in range(len(keyList)):
                    self.keyboard.release(keyList[-i-1])

            elif function == "Script":
                script = self.mainWindow.getKey(profile, key).scriptLocation
                if script:
                    try:
                        runpy.run_path(script)
                    except Exception as e:
                        self.mainWindow.getKey(profile, key).scriptLabel.setText("Error")
                        print(e)

        # handle encoder input
        elif data.startswith("E"):
            # get encoder function
            encoder = int(data[1])
            mode = data[2]
            if mode == "+":
                function = self.mainWindow.getEncoder(profile, encoder).cwFunction.currentText()
            elif mode == "-":
                function = self.mainWindow.getEncoder(profile, encoder).ccwFunction.currentText()
            elif mode == "S":
                if encoder == 0:
                    function = "Prev profile"
                elif encoder == 1:
                    function = "Next profile"

            # execute function
            if function == "None":
                return
            
            elif function == "Volume +":
                self.keyboard.tap(Key.media_volume_up)

            elif function == "Volume -":
                self.keyboard.tap(Key.media_volume_down)

            elif function == "Scroll up":
                self.mouse.scroll(0, 1)

            elif function == "Scroll down":
                self.mouse.scroll(0, -1)

            elif function == "Scroll left":
                self.mouse.scroll(-1, 0)

            elif function == "Scroll right":
                self.mouse.scroll(1, 0)  

            elif function == "Prev profile":
                if profile == 0:
                    self.mainWindow.activeProfile = profileCount - 1
                else:
                    self.mainWindow.activeProfile -= 1
                print(self.mainWindow.activeProfile)
            
            elif function == "Next profile":
                if profile == profileCount - 1:
                    self.mainWindow.activeProfile = 0
                else:
                    self.mainWindow.activeProfile += 1
                print(self.mainWindow.activeProfile)

    def uploadSettings(self):
        delay = 0.005
        profileCount = self.mainWindow.tabWidget.count() - 1

        self.mainWindow.portStatus.setText("Uploading...")

        # send settings via UART
        self.serialPort.write("0".encode())                 # set active profile to 0
        time.sleep(delay)
        self.serialPort.write(str(profileCount).encode())   # send profile count
        time.sleep(delay)

        # send profiles
        for p in range(profileCount + 1):
            print(f"Uploading profile {p}.")
            profile = self.mainWindow.tabWidget.tabText(p).ljust(12)        # get profile name (12 chars)
            for k in range(12):
                key = self.mainWindow.getKey(p, k).keyName.text().ljust(6)  # get key names (6 chars)
                profile = profile + key
            for i in range(len(profile)):
                time.sleep(delay)
                self.serialPort.write(profile[i].encode())  # write profile chars one by one            

        time.sleep(0.1)
        self.serialPort.reset_input_buffer()
        self.mainWindow.uploading = False
        self.mainWindow.portStatus.setText("Connected")
        self.mainWindow.activeProfile = 0
                        


class PortFinder(QObject):

    deviceConnected = pyqtSignal()

    def __init__(self, main_window):
        super().__init__()
        self.mainWindow = main_window   # reference to main app window    

    def run(self):
        found = False

        while True:
            found = False

            # look through connected ports whether the device is connected or not
            for comport in listPorts.comports():
                if comport.hwid == "USB VID:PID=0403:6001 SER=A50285BIA":
                    found = True
                    break 

            # device found and not connected -> connect -> start port reader
            if found and not self.mainWindow.deviceConnected:
                self.mainWindow.port.setText(comport.name)
                self.mainWindow.portStatus.setText("Connecting...")
                self.deviceConnected.emit()
                break             

            time.sleep(0.1)