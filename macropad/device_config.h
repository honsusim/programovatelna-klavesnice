#ifndef DEVICE_CONFIG_H
#define	DEVICE_CONFIG_H
#include <xc.h>
#include "pin_manager.h"
#include "uart.h"
#include "sh1106.h"
#include "profile.h"
#include "led.h"

#define _XTAL_FREQ 64000000

void SYSTEM_Initialize(void);
void OSCILLATOR_Initialize(void);
void PMD_Initialize(void);

#endif
