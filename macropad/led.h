#ifndef LED_H
#define	LED_H
#include <xc.h>
#include "pin_manager.h"
#include "device_config.h"

void LED_SetSingle(uint8_t r, uint8_t g, uint8_t b); 
void LED_SetAll(uint8_t r, uint8_t g, uint8_t b);

#endif

