#include "led.h"

void LED_SetSingle(uint8_t r, uint8_t g, uint8_t b)
{
    uint8_t mask = 0x01;
    uint8_t colors[3] = {g, r, b};
    
    // cycle through colors
    for(uint8_t c = 0; c < 3; c++)
    {
        // cycle through individual bits (MSM first)
        for(uint8_t i = 0; i < 8; i++)
        {
            uint8_t bit = (colors[c] >> (7-i)) & mask;
            if(bit)
            {
                LED_SetHigh();
                __delay_us(0.64);
                LED_SetLow();
                __delay_us(0.68);
            }
            else
            {
                LED_SetHigh();
                __delay_us(0.32);
                LED_SetLow();
                __delay_us(1);                
            }        
        }
    }    
}

void LED_SetAll(uint8_t r, uint8_t g, uint8_t b)
{
    for(uint8_t i = 0; i < 12; i++)
        LED_SetSingle(r, g, b);
    __delay_us(100);
}