#include "pin_manager.h"

void PIN_MANAGER_Initialize(void)
{    
    // LATx registers (pin output)
    LATA = 0x00;
    LATB = 0x00;
    LATC = 0x00;
    
    // TRISx registers (input/output)
    TRISA = 0xFF;
    TRISB = 0x83;
    TRISC = 0xF7;
    
    // ANSELx registers (analog/digital)
    ANSELC = 0x00;
    ANSELB = 0x00;
    ANSELA = 0x00;
    
    // WPUx registers (weak pull-ups)
    /*
    WPUA = 0xFF;
    WPUB = 0b00000011;
    WPUC = 0xF7;
    WPUE = 0x08;
    */
    
    // RxyI2C registers
    RB1I2C = 0x00;
    RB2I2C = 0x00;
    RC3I2C = 0x00;
    RC4I2C = 0x00;
    
    // ODx registers
    ODCONA = 0x00;
    ODCONB = 0x00;
    ODCONC = 0x00;
    
    // SLRCONx registers
    SLRCONA = 0xFF;
    SLRCONB = 0xFF;
    SLRCONC = 0xFF;
    
    // INLVLx registers
    INLVLA = 0xFF;
    INLVLB = 0xFF;
    INLVLC = 0xFF;
    INLVLE = 0x08;
    
    // PPS registers (peripheral pin select)
    U1RXPPS = 0b00001111;   // RB7 -> UART1 RX
    RB6PPS = 0b00010011;    // RB6 -> UART1 TX
    RB2PPS = 0b00011110;    // RC4 -> SPI1 SCK 
    RB3PPS = 0b00011111;    // RC5 -> SPI1 SDO
}
