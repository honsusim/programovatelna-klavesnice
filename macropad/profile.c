#include "profile.h"
#include "led.h"

void PROFILE_Initialize(void)
{
    profile_active = EEPROM_Read(0);
    profile_count = EEPROM_Read(1);     
    
    // if the EEPROM isn't initialized, return
    if(profile_count > 9)
    {
        LED_SetAll(0, 0, 50);
        return;        
    }        
    
    // load profiles
    for(uint8_t p = 0; p <= profile_count; p++)
    {
        // load profile name
        for(uint8_t i = 0; i < 12; i++)
        {
            // profiles[p].profile_name[i] = EEPROM_Read(2 + 84*p + i);
            profiles[p].profile_name[i] = EEPROM_Read(2 + 84*p + i);
        }
        
        // load key names
        for(uint8_t k = 0; k < 12; k++)
        {
            for(uint8_t i = 0; i < 12; i++)
            {
                profiles[p].key_names[k][i] = EEPROM_Read(2 + 84*p + 12 + 6*k + i);
            }
        }
    }   
}

void PROFILE_Change(uint8_t dir)
{
    if(dir == PREV)
    {
        if(profile_active == 0)
        {
            profile_active = profile_count;
            SH1106_DisplayProfile(profile_active);
        }
        else
        {
            profile_active--;
            SH1106_DisplayProfile(profile_active);
        }
    }
    else if(dir == NEXT)
    {
        if(profile_active == profile_count)
        {
            profile_active = 0;
            SH1106_DisplayProfile(profile_active);
        }
        else
        {
            profile_active++;
            SH1106_DisplayProfile(profile_active);
        }        
    }
    EEPROM_Write(0, profile_active);
}