#ifndef SH1106_FONT_H
#define	SH1106_FONT_H

#include <xc.h>

#define FONT_WIDTH  6
#define FONT_HEIGHT 8

extern const uint8_t font[];

#endif

