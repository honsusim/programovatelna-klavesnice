/* HOW TO USE
 * written using SH1106 and SSD1306 manuals
 * DC, SDO, SCK, RES must be set as digital outputs
 * SDO, SCK must be mapped to according pins using PPS registers
 * DC, RES pins have to be defined using pin manager (functions)
 * SPI1CON0 EN bit must be set to 1 after the initial configuration
 * connect CS pin to GND
 * 
 *  |-----------------------|
 *  | profile name          |
 *  |-------|-------|-------|
 *  | key0  | key1  | key2  |
 *  |-------|-------|-------|
 *  | key3  | key4  | key5  |
 *  |-------|-------|-------|
 *  | key6  | key7  | key8  |
 *  |-------|-------|-------|
 *  | key9  | key10 | key11 |
 *  |-------|-------|-------|
 * 
 */

#ifndef SH1106_H
#define	SH1106_H

#include <xc.h>
#include <string.h>
#include "pin_manager.h"
#include "device_config.h"
#include "sh1106_font.h"
#include "profile.h"

// command definitions
#define CMD_DISPLAY_ON      0xAF
#define CMD_DISPLAY_OFF     0xAE
#define CMD_SET_CONTRAST    0x81
#define CMD_FLIP_HOR        0xA1
#define CMD_FLIP_VERT       0xC8

// draw pixel colors
#define BLACK   0
#define WHITE   1
#define INVERSE 2

// function declarations
void SPI_Initialize(void);
void SH1106_Initialize(void);
void SH1106_WriteCommand(uint8_t command);
void SH1106_WriteData(uint8_t data);
void SH1106_SetContrast(uint8_t contrast);
void SH1106_Goto(uint8_t page, uint8_t column);
void SH1106_ClearBuffer(void);
void SH1106_DisplayBuffer(void);
void SH1106_DrawPixel(uint8_t x, uint8_t y, uint8_t color);
void SH1106_DrawRectangle(uint8_t x, uint8_t y, uint8_t h, uint8_t w);
void SH1106_DrawChar(uint8_t x, uint8_t y, char c, uint8_t color);
void SH1106_DrawText(uint8_t x, uint8_t y, char text[], uint8_t length, uint8_t color);
void SH1106_DisplayProfile(uint8_t profile);
void SH1106_InvertKey(uint8_t key_number);

#endif