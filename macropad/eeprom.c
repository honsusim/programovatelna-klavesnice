#include "eeprom.h"

void EEPROM_Write(uint16_t address, uint8_t data)
{
    // EEPROM addresses are <0x0000;0x03FF> - (0;1023)
    
    while (NVMCON1bits.WR);             // wait for previous write to complete
    NVMADRL = address & 0xFF;           // set lower byte of the address
    NVMADRH = (address >> 8) & 0xFF;    // set higher byte of the address
    NVMDAT = data;                      // set write data
    NVMCON1bits.NVMREG = 0;             // select EEPROM memory
    NVMCON1bits.WREN = 1;               // enable writes
    
    // unlock sequence
    NVMCON2 = 0x55;
    NVMCON2 = 0xAA;
    NVMCON1bits.WR = 1; // start write    
}

uint8_t EEPROM_Read(uint16_t address)
{
    // EEPROM addresses are <0x0000;0x03FF> - (0;1023)
    
    while (NVMCON1bits.RD);             // wait for previous read to complete
    NVMADRL = address & 0xFF;           // set lower byte of the address
    NVMADRH = (address >> 8) & 0xFF;    // set higher byte of the address
    NVMCON1bits.NVMREG = 0;             // select EEPROM memory
    NVMCON1bits.RD = 1;                 // start read
    return NVMDAT;                      // return read data
}