#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H
#include <xc.h>

// get/set KEY_01 aliases
#define KEY_01_TRIS                 TRISAbits.TRISA0
#define KEY_01_LAT                  LATAbits.LATA0
#define KEY_01_PORT                 PORTAbits.RA0
#define KEY_01_WPU                  WPUAbits.WPUA0
#define KEY_01_OD                   ODCONAbits.ODCA0
#define KEY_01_ANS                  ANSELAbits.ANSELA0
#define KEY_01_SetHigh()            do { LATAbits.LATA0 = 1; } while(0)
#define KEY_01_SetLow()             do { LATAbits.LATA0 = 0; } while(0)
#define KEY_01_Toggle()             do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
#define KEY_01_GetValue()           PORTAbits.RA0
#define KEY_01_SetDigitalInput()    do { TRISAbits.TRISA0 = 1; } while(0)
#define KEY_01_SetDigitalOutput()   do { TRISAbits.TRISA0 = 0; } while(0)
#define KEY_01_SetPullup()          do { WPUAbits.WPUA0 = 1; } while(0)
#define KEY_01_ResetPullup()        do { WPUAbits.WPUA0 = 0; } while(0)
#define KEY_01_SetPushPull()        do { ODCONAbits.ODCA0 = 0; } while(0)
#define KEY_01_SetOpenDrain()       do { ODCONAbits.ODCA0 = 1; } while(0)
#define KEY_01_SetAnalogMode()      do { ANSELAbits.ANSELA0 = 1; } while(0)
#define KEY_01_SetDigitalMode()     do { ANSELAbits.ANSELA0 = 0; } while(0)

// get/set KEY_02 aliases
#define KEY_02_TRIS                 TRISAbits.TRISA1
#define KEY_02_LAT                  LATAbits.LATA1
#define KEY_02_PORT                 PORTAbits.RA1
#define KEY_02_WPU                  WPUAbits.WPUA1
#define KEY_02_OD                   ODCONAbits.ODCA1
#define KEY_02_ANS                  ANSELAbits.ANSELA1
#define KEY_02_SetHigh()            do { LATAbits.LATA1 = 1; } while(0)
#define KEY_02_SetLow()             do { LATAbits.LATA1 = 0; } while(0)
#define KEY_02_Toggle()             do { LATAbits.LATA1 = ~LATAbits.LATA1; } while(0)
#define KEY_02_GetValue()           PORTAbits.RA1
#define KEY_02_SetDigitalInput()    do { TRISAbits.TRISA1 = 1; } while(0)
#define KEY_02_SetDigitalOutput()   do { TRISAbits.TRISA1 = 0; } while(0)
#define KEY_02_SetPullup()          do { WPUAbits.WPUA1 = 1; } while(0)
#define KEY_02_ResetPullup()        do { WPUAbits.WPUA1 = 0; } while(0)
#define KEY_02_SetPushPull()        do { ODCONAbits.ODCA1 = 0; } while(0)
#define KEY_02_SetOpenDrain()       do { ODCONAbits.ODCA1 = 1; } while(0)
#define KEY_02_SetAnalogMode()      do { ANSELAbits.ANSELA1 = 1; } while(0)
#define KEY_02_SetDigitalMode()     do { ANSELAbits.ANSELA1 = 0; } while(0)

// get/set KEY_03 aliases
#define KEY_03_TRIS                 TRISAbits.TRISA2
#define KEY_03_LAT                  LATAbits.LATA2
#define KEY_03_PORT                 PORTAbits.RA2
#define KEY_03_WPU                  WPUAbits.WPUA2
#define KEY_03_OD                   ODCONAbits.ODCA2
#define KEY_03_ANS                  ANSELAbits.ANSELA2
#define KEY_03_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define KEY_03_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define KEY_03_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define KEY_03_GetValue()           PORTAbits.RA2
#define KEY_03_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define KEY_03_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define KEY_03_SetPullup()          do { WPUAbits.WPUA2 = 1; } while(0)
#define KEY_03_ResetPullup()        do { WPUAbits.WPUA2 = 0; } while(0)
#define KEY_03_SetPushPull()        do { ODCONAbits.ODCA2 = 0; } while(0)
#define KEY_03_SetOpenDrain()       do { ODCONAbits.ODCA2 = 1; } while(0)
#define KEY_03_SetAnalogMode()      do { ANSELAbits.ANSELA2 = 1; } while(0)
#define KEY_03_SetDigitalMode()     do { ANSELAbits.ANSELA2 = 0; } while(0)

// get/set KEY_04 aliases
#define KEY_04_TRIS                 TRISAbits.TRISA3
#define KEY_04_LAT                  LATAbits.LATA3
#define KEY_04_PORT                 PORTAbits.RA3
#define KEY_04_WPU                  WPUAbits.WPUA3
#define KEY_04_OD                   ODCONAbits.ODCA3
#define KEY_04_ANS                  ANSELAbits.ANSELA3
#define KEY_04_SetHigh()            do { LATAbits.LATA3 = 1; } while(0)
#define KEY_04_SetLow()             do { LATAbits.LATA3 = 0; } while(0)
#define KEY_04_Toggle()             do { LATAbits.LATA3 = ~LATAbits.LATA3; } while(0)
#define KEY_04_GetValue()           PORTAbits.RA3
#define KEY_04_SetDigitalInput()    do { TRISAbits.TRISA3 = 1; } while(0)
#define KEY_04_SetDigitalOutput()   do { TRISAbits.TRISA3 = 0; } while(0)
#define KEY_04_SetPullup()          do { WPUAbits.WPUA3 = 1; } while(0)
#define KEY_04_ResetPullup()        do { WPUAbits.WPUA3 = 0; } while(0)
#define KEY_04_SetPushPull()        do { ODCONAbits.ODCA3 = 0; } while(0)
#define KEY_04_SetOpenDrain()       do { ODCONAbits.ODCA3 = 1; } while(0)
#define KEY_04_SetAnalogMode()      do { ANSELAbits.ANSELA3 = 1; } while(0)
#define KEY_04_SetDigitalMode()     do { ANSELAbits.ANSELA3 = 0; } while(0)

// get/set KEY_05 aliases
#define KEY_05_TRIS                 TRISAbits.TRISA4
#define KEY_05_LAT                  LATAbits.LATA4
#define KEY_05_PORT                 PORTAbits.RA4
#define KEY_05_WPU                  WPUAbits.WPUA4
#define KEY_05_OD                   ODCONAbits.ODCA4
#define KEY_05_ANS                  ANSELAbits.ANSELA4
#define KEY_05_SetHigh()            do { LATAbits.LATA4 = 1; } while(0)
#define KEY_05_SetLow()             do { LATAbits.LATA4 = 0; } while(0)
#define KEY_05_Toggle()             do { LATAbits.LATA4 = ~LATAbits.LATA4; } while(0)
#define KEY_05_GetValue()           PORTAbits.RA4
#define KEY_05_SetDigitalInput()    do { TRISAbits.TRISA4 = 1; } while(0)
#define KEY_05_SetDigitalOutput()   do { TRISAbits.TRISA4 = 0; } while(0)
#define KEY_05_SetPullup()          do { WPUAbits.WPUA4 = 1; } while(0)
#define KEY_05_ResetPullup()        do { WPUAbits.WPUA4 = 0; } while(0)
#define KEY_05_SetPushPull()        do { ODCONAbits.ODCA4 = 0; } while(0)
#define KEY_05_SetOpenDrain()       do { ODCONAbits.ODCA4 = 1; } while(0)
#define KEY_05_SetAnalogMode()      do { ANSELAbits.ANSELA4 = 1; } while(0)
#define KEY_05_SetDigitalMode()     do { ANSELAbits.ANSELA4 = 0; } while(0)

// get/set KEY_06 aliases
#define KEY_06_TRIS                 TRISAbits.TRISA5
#define KEY_06_LAT                  LATAbits.LATA5
#define KEY_06_PORT                 PORTAbits.RA5
#define KEY_06_WPU                  WPUAbits.WPUA5
#define KEY_06_OD                   ODCONAbits.ODCA5
#define KEY_06_ANS                  ANSELAbits.ANSELA5
#define KEY_06_SetHigh()            do { LATAbits.LATA5 = 1; } while(0)
#define KEY_06_SetLow()             do { LATAbits.LATA5 = 0; } while(0)
#define KEY_06_Toggle()             do { LATAbits.LATA5 = ~LATAbits.LATA5; } while(0)
#define KEY_06_GetValue()           PORTAbits.RA5
#define KEY_06_SetDigitalInput()    do { TRISAbits.TRISA5 = 1; } while(0)
#define KEY_06_SetDigitalOutput()   do { TRISAbits.TRISA5 = 0; } while(0)
#define KEY_06_SetPullup()          do { WPUAbits.WPUA5 = 1; } while(0)
#define KEY_06_ResetPullup()        do { WPUAbits.WPUA5 = 0; } while(0)
#define KEY_06_SetPushPull()        do { ODCONAbits.ODCA5 = 0; } while(0)
#define KEY_06_SetOpenDrain()       do { ODCONAbits.ODCA5 = 1; } while(0)
#define KEY_06_SetAnalogMode()      do { ANSELAbits.ANSELA5 = 1; } while(0)
#define KEY_06_SetDigitalMode()     do { ANSELAbits.ANSELA5 = 0; } while(0)

// get/set KEY_08 aliases
#define KEY_08_TRIS                 TRISAbits.TRISA6
#define KEY_08_LAT                  LATAbits.LATA6
#define KEY_08_PORT                 PORTAbits.RA6
#define KEY_08_WPU                  WPUAbits.WPUA6
#define KEY_08_OD                   ODCONAbits.ODCA6
#define KEY_08_ANS                  ANSELAbits.ANSELA6
#define KEY_08_SetHigh()            do { LATAbits.LATA6 = 1; } while(0)
#define KEY_08_SetLow()             do { LATAbits.LATA6 = 0; } while(0)
#define KEY_08_Toggle()             do { LATAbits.LATA6 = ~LATAbits.LATA6; } while(0)
#define KEY_08_GetValue()           PORTAbits.RA6
#define KEY_08_SetDigitalInput()    do { TRISAbits.TRISA6 = 1; } while(0)
#define KEY_08_SetDigitalOutput()   do { TRISAbits.TRISA6 = 0; } while(0)
#define KEY_08_SetPullup()          do { WPUAbits.WPUA6 = 1; } while(0)
#define KEY_08_ResetPullup()        do { WPUAbits.WPUA6 = 0; } while(0)
#define KEY_08_SetPushPull()        do { ODCONAbits.ODCA6 = 0; } while(0)
#define KEY_08_SetOpenDrain()       do { ODCONAbits.ODCA6 = 1; } while(0)
#define KEY_08_SetAnalogMode()      do { ANSELAbits.ANSELA6 = 1; } while(0)
#define KEY_08_SetDigitalMode()     do { ANSELAbits.ANSELA6 = 0; } while(0)

// get/set KEY_07 aliases
#define KEY_07_TRIS                 TRISAbits.TRISA7
#define KEY_07_LAT                  LATAbits.LATA7
#define KEY_07_PORT                 PORTAbits.RA7
#define KEY_07_WPU                  WPUAbits.WPUA7
#define KEY_07_OD                   ODCONAbits.ODCA7
#define KEY_07_ANS                  ANSELAbits.ANSELA7
#define KEY_07_SetHigh()            do { LATAbits.LATA7 = 1; } while(0)
#define KEY_07_SetLow()             do { LATAbits.LATA7 = 0; } while(0)
#define KEY_07_Toggle()             do { LATAbits.LATA7 = ~LATAbits.LATA7; } while(0)
#define KEY_07_GetValue()           PORTAbits.RA7
#define KEY_07_SetDigitalInput()    do { TRISAbits.TRISA7 = 1; } while(0)
#define KEY_07_SetDigitalOutput()   do { TRISAbits.TRISA7 = 0; } while(0)
#define KEY_07_SetPullup()          do { WPUAbits.WPUA7 = 1; } while(0)
#define KEY_07_ResetPullup()        do { WPUAbits.WPUA7 = 0; } while(0)
#define KEY_07_SetPushPull()        do { ODCONAbits.ODCA7 = 0; } while(0)
#define KEY_07_SetOpenDrain()       do { ODCONAbits.ODCA7 = 1; } while(0)
#define KEY_07_SetAnalogMode()      do { ANSELAbits.ANSELA7 = 1; } while(0)
#define KEY_07_SetDigitalMode()     do { ANSELAbits.ANSELA7 = 0; } while(0)

// get/set ENC1_B aliases
#define ENC1_B_TRIS                 TRISBbits.TRISB0
#define ENC1_B_LAT                  LATBbits.LATB0
#define ENC1_B_PORT                 PORTBbits.RB0
#define ENC1_B_WPU                  WPUBbits.WPUB0
#define ENC1_B_OD                   ODCONBbits.ODCB0
#define ENC1_B_ANS                  ANSELBbits.ANSELB0
#define ENC1_B_SetHigh()            do { LATBbits.LATB0 = 1; } while(0)
#define ENC1_B_SetLow()             do { LATBbits.LATB0 = 0; } while(0)
#define ENC1_B_Toggle()             do { LATBbits.LATB0 = ~LATBbits.LATB0; } while(0)
#define ENC1_B_GetValue()           PORTBbits.RB0
#define ENC1_B_SetDigitalInput()    do { TRISBbits.TRISB0 = 1; } while(0)
#define ENC1_B_SetDigitalOutput()   do { TRISBbits.TRISB0 = 0; } while(0)
#define ENC1_B_SetPullup()          do { WPUBbits.WPUB0 = 1; } while(0)
#define ENC1_B_ResetPullup()        do { WPUBbits.WPUB0 = 0; } while(0)
#define ENC1_B_SetPushPull()        do { ODCONBbits.ODCB0 = 0; } while(0)
#define ENC1_B_SetOpenDrain()       do { ODCONBbits.ODCB0 = 1; } while(0)
#define ENC1_B_SetAnalogMode()      do { ANSELBbits.ANSELB0 = 1; } while(0)
#define ENC1_B_SetDigitalMode()     do { ANSELBbits.ANSELB0 = 0; } while(0)

// get/set ENC1_S aliases
#define ENC1_S_TRIS                 TRISBbits.TRISB1
#define ENC1_S_LAT                  LATBbits.LATB1
#define ENC1_S_PORT                 PORTBbits.RB1
#define ENC1_S_WPU                  WPUBbits.WPUB1
#define ENC1_S_OD                   ODCONBbits.ODCB1
#define ENC1_S_ANS                  ANSELBbits.ANSELB1
#define ENC1_S_SetHigh()            do { LATBbits.LATB1 = 1; } while(0)
#define ENC1_S_SetLow()             do { LATBbits.LATB1 = 0; } while(0)
#define ENC1_S_Toggle()             do { LATBbits.LATB1 = ~LATBbits.LATB1; } while(0)
#define ENC1_S_GetValue()           PORTBbits.RB1
#define ENC1_S_SetDigitalInput()    do { TRISBbits.TRISB1 = 1; } while(0)
#define ENC1_S_SetDigitalOutput()   do { TRISBbits.TRISB1 = 0; } while(0)
#define ENC1_S_SetPullup()          do { WPUBbits.WPUB1 = 1; } while(0)
#define ENC1_S_ResetPullup()        do { WPUBbits.WPUB1 = 0; } while(0)
#define ENC1_S_SetPushPull()        do { ODCONBbits.ODCB1 = 0; } while(0)
#define ENC1_S_SetOpenDrain()       do { ODCONBbits.ODCB1 = 1; } while(0)
#define ENC1_S_SetAnalogMode()      do { ANSELBbits.ANSELB1 = 1; } while(0)
#define ENC1_S_SetDigitalMode()     do { ANSELBbits.ANSELB1 = 0; } while(0)

// get/set SH1106_SCK aliases
#define SH1106_SCK_TRIS                 TRISBbits.TRISB2
#define SH1106_SCK_LAT                  LATBbits.LATB2
#define SH1106_SCK_PORT                 PORTBbits.RB2
#define SH1106_SCK_WPU                  WPUBbits.WPUB2
#define SH1106_SCK_OD                   ODCONBbits.ODCB2
#define SH1106_SCK_ANS                  ANSELBbits.ANSELB2
#define SH1106_SCK_SetHigh()            do { LATBbits.LATB2 = 1; } while(0)
#define SH1106_SCK_SetLow()             do { LATBbits.LATB2 = 0; } while(0)
#define SH1106_SCK_Toggle()             do { LATBbits.LATB2 = ~LATBbits.LATB2; } while(0)
#define SH1106_SCK_GetValue()           PORTBbits.RB2
#define SH1106_SCK_SetDigitalInput()    do { TRISBbits.TRISB2 = 1; } while(0)
#define SH1106_SCK_SetDigitalOutput()   do { TRISBbits.TRISB2 = 0; } while(0)
#define SH1106_SCK_SetPullup()          do { WPUBbits.WPUB2 = 1; } while(0)
#define SH1106_SCK_ResetPullup()        do { WPUBbits.WPUB2 = 0; } while(0)
#define SH1106_SCK_SetPushPull()        do { ODCONBbits.ODCB2 = 0; } while(0)
#define SH1106_SCK_SetOpenDrain()       do { ODCONBbits.ODCB2 = 1; } while(0)
#define SH1106_SCK_SetAnalogMode()      do { ANSELBbits.ANSELB2 = 1; } while(0)
#define SH1106_SCK_SetDigitalMode()     do { ANSELBbits.ANSELB2 = 0; } while(0)

// get/set SH1106_SDO aliases
#define SH1106_SDO_TRIS                 TRISBbits.TRISB3
#define SH1106_SDO_LAT                  LATBbits.LATB3
#define SH1106_SDO_PORT                 PORTBbits.RB3
#define SH1106_SDO_WPU                  WPUBbits.WPUB3
#define SH1106_SDO_OD                   ODCONBbits.ODCB3
#define SH1106_SDO_ANS                  ANSELBbits.ANSELB3
#define SH1106_SDO_SetHigh()            do { LATBbits.LATB3 = 1; } while(0)
#define SH1106_SDO_SetLow()             do { LATBbits.LATB3 = 0; } while(0)
#define SH1106_SDO_Toggle()             do { LATBbits.LATB3 = ~LATBbits.LATB3; } while(0)
#define SH1106_SDO_GetValue()           PORTBbits.RB3
#define SH1106_SDO_SetDigitalInput()    do { TRISBbits.TRISB3 = 1; } while(0)
#define SH1106_SDO_SetDigitalOutput()   do { TRISBbits.TRISB3 = 0; } while(0)
#define SH1106_SDO_SetPullup()          do { WPUBbits.WPUB3 = 1; } while(0)
#define SH1106_SDO_ResetPullup()        do { WPUBbits.WPUB3 = 0; } while(0)
#define SH1106_SDO_SetPushPull()        do { ODCONBbits.ODCB3 = 0; } while(0)
#define SH1106_SDO_SetOpenDrain()       do { ODCONBbits.ODCB3 = 1; } while(0)
#define SH1106_SDO_SetAnalogMode()      do { ANSELBbits.ANSELB3 = 1; } while(0)
#define SH1106_SDO_SetDigitalMode()     do { ANSELBbits.ANSELB3 = 0; } while(0)

// get/set SH1106_RES aliases
#define SH1106_RES_TRIS                 TRISBbits.TRISB4
#define SH1106_RES_LAT                  LATBbits.LATB4
#define SH1106_RES_PORT                 PORTBbits.RB4
#define SH1106_RES_WPU                  WPUBbits.WPUB4
#define SH1106_RES_OD                   ODCONBbits.ODCB4
#define SH1106_RES_ANS                  ANSELBbits.ANSELB4
#define SH1106_RES_SetHigh()            do { LATBbits.LATB4 = 1; } while(0)
#define SH1106_RES_SetLow()             do { LATBbits.LATB4 = 0; } while(0)
#define SH1106_RES_Toggle()             do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define SH1106_RES_GetValue()           PORTBbits.RB4
#define SH1106_RES_SetDigitalInput()    do { TRISBbits.TRISB4 = 1; } while(0)
#define SH1106_RES_SetDigitalOutput()   do { TRISBbits.TRISB4 = 0; } while(0)
#define SH1106_RES_SetPullup()          do { WPUBbits.WPUB4 = 1; } while(0)
#define SH1106_RES_ResetPullup()        do { WPUBbits.WPUB4 = 0; } while(0)
#define SH1106_RES_SetPushPull()        do { ODCONBbits.ODCB4 = 0; } while(0)
#define SH1106_RES_SetOpenDrain()       do { ODCONBbits.ODCB4 = 1; } while(0)
#define SH1106_RES_SetAnalogMode()      do { ANSELBbits.ANSELB4 = 1; } while(0)
#define SH1106_RES_SetDigitalMode()     do { ANSELBbits.ANSELB4 = 0; } while(0)

// get/set SH1106_DC aliases
#define SH1106_DC_TRIS                 TRISBbits.TRISB5
#define SH1106_DC_LAT                  LATBbits.LATB5
#define SH1106_DC_PORT                 PORTBbits.RB5
#define SH1106_DC_WPU                  WPUBbits.WPUB5
#define SH1106_DC_OD                   ODCONBbits.ODCB5
#define SH1106_DC_ANS                  ANSELBbits.ANSELB5
#define SH1106_DC_SetHigh()            do { LATBbits.LATB5 = 1; } while(0)
#define SH1106_DC_SetLow()             do { LATBbits.LATB5 = 0; } while(0)
#define SH1106_DC_Toggle()             do { LATBbits.LATB5 = ~LATBbits.LATB5; } while(0)
#define SH1106_DC_GetValue()           PORTBbits.RB5
#define SH1106_DC_SetDigitalInput()    do { TRISBbits.TRISB5 = 1; } while(0)
#define SH1106_DC_SetDigitalOutput()   do { TRISBbits.TRISB5 = 0; } while(0)
#define SH1106_DC_SetPullup()          do { WPUBbits.WPUB5 = 1; } while(0)
#define SH1106_DC_ResetPullup()        do { WPUBbits.WPUB5 = 0; } while(0)
#define SH1106_DC_SetPushPull()        do { ODCONBbits.ODCB5 = 0; } while(0)
#define SH1106_DC_SetOpenDrain()       do { ODCONBbits.ODCB5 = 1; } while(0)
#define SH1106_DC_SetAnalogMode()      do { ANSELBbits.ANSELB5 = 1; } while(0)
#define SH1106_DC_SetDigitalMode()     do { ANSELBbits.ANSELB5 = 0; } while(0)

// get/set UART_TX aliases
#define UART_TX_TRIS                 TRISBbits.TRISB6
#define UART_TX_LAT                  LATBbits.LATB6
#define UART_TX_PORT                 PORTBbits.RB6
#define UART_TX_WPU                  WPUBbits.WPUB6
#define UART_TX_OD                   ODCONBbits.ODCB6
#define UART_TX_ANS                  ANSELBbits.ANSELB6
#define UART_TX_SetHigh()            do { LATBbits.LATB6 = 1; } while(0)
#define UART_TX_SetLow()             do { LATBbits.LATB6 = 0; } while(0)
#define UART_TX_Toggle()             do { LATBbits.LATB6 = ~LATBbits.LATB6; } while(0)
#define UART_TX_GetValue()           PORTBbits.RB6
#define UART_TX_SetDigitalInput()    do { TRISBbits.TRISB6 = 1; } while(0)
#define UART_TX_SetDigitalOutput()   do { TRISBbits.TRISB6 = 0; } while(0)
#define UART_TX_SetPullup()          do { WPUBbits.WPUB6 = 1; } while(0)
#define UART_TX_ResetPullup()        do { WPUBbits.WPUB6 = 0; } while(0)
#define UART_TX_SetPushPull()        do { ODCONBbits.ODCB6 = 0; } while(0)
#define UART_TX_SetOpenDrain()       do { ODCONBbits.ODCB6 = 1; } while(0)
#define UART_TX_SetAnalogMode()      do { ANSELBbits.ANSELB6 = 1; } while(0)
#define UART_TX_SetDigitalMode()     do { ANSELBbits.ANSELB6 = 0; } while(0)

// get/set UART_RX aliases
#define UART_RX_TRIS                 TRISBbits.TRISB7
#define UART_RX_LAT                  LATBbits.LATB7
#define UART_RX_PORT                 PORTBbits.RB7
#define UART_RX_WPU                  WPUBbits.WPUB7
#define UART_RX_OD                   ODCONBbits.ODCB7
#define UART_RX_ANS                  ANSELBbits.ANSELB7
#define UART_RX_SetHigh()            do { LATBbits.LATB7 = 1; } while(0)
#define UART_RX_SetLow()             do { LATBbits.LATB7 = 0; } while(0)
#define UART_RX_Toggle()             do { LATBbits.LATB7 = ~LATBbits.LATB7; } while(0)
#define UART_RX_GetValue()           PORTBbits.RB7
#define UART_RX_SetDigitalInput()    do { TRISBbits.TRISB7 = 1; } while(0)
#define UART_RX_SetDigitalOutput()   do { TRISBbits.TRISB7 = 0; } while(0)
#define UART_RX_SetPullup()          do { WPUBbits.WPUB7 = 1; } while(0)
#define UART_RX_ResetPullup()        do { WPUBbits.WPUB7 = 0; } while(0)
#define UART_RX_SetPushPull()        do { ODCONBbits.ODCB7 = 0; } while(0)
#define UART_RX_SetOpenDrain()       do { ODCONBbits.ODCB7 = 1; } while(0)
#define UART_RX_SetAnalogMode()      do { ANSELBbits.ANSELB7 = 1; } while(0)
#define UART_RX_SetDigitalMode()     do { ANSELBbits.ANSELB7 = 0; } while(0)

// get/set KEY_09 aliases
#define KEY_09_TRIS                 TRISCbits.TRISC0
#define KEY_09_LAT                  LATCbits.LATC0
#define KEY_09_PORT                 PORTCbits.RC0
#define KEY_09_WPU                  WPUCbits.WPUC0
#define KEY_09_OD                   ODCONCbits.ODCC0
#define KEY_09_ANS                  ANSELCbits.ANSELC0
#define KEY_09_SetHigh()            do { LATCbits.LATC0 = 1; } while(0)
#define KEY_09_SetLow()             do { LATCbits.LATC0 = 0; } while(0)
#define KEY_09_Toggle()             do { LATCbits.LATC0 = ~LATCbits.LATC0; } while(0)
#define KEY_09_GetValue()           PORTCbits.RC0
#define KEY_09_SetDigitalInput()    do { TRISCbits.TRISC0 = 1; } while(0)
#define KEY_09_SetDigitalOutput()   do { TRISCbits.TRISC0 = 0; } while(0)
#define KEY_09_SetPullup()          do { WPUCbits.WPUC0 = 1; } while(0)
#define KEY_09_ResetPullup()        do { WPUCbits.WPUC0 = 0; } while(0)
#define KEY_09_SetPushPull()        do { ODCONCbits.ODCC0 = 0; } while(0)
#define KEY_09_SetOpenDrain()       do { ODCONCbits.ODCC0 = 1; } while(0)
#define KEY_09_SetAnalogMode()      do { ANSELCbits.ANSELC0 = 1; } while(0)
#define KEY_09_SetDigitalMode()     do { ANSELCbits.ANSELC0 = 0; } while(0)

// get/set KEY_10 aliases
#define KEY_10_TRIS                 TRISCbits.TRISC1
#define KEY_10_LAT                  LATCbits.LATC1
#define KEY_10_PORT                 PORTCbits.RC1
#define KEY_10_WPU                  WPUCbits.WPUC1
#define KEY_10_OD                   ODCONCbits.ODCC1
#define KEY_10_ANS                  ANSELCbits.ANSELC1
#define KEY_10_SetHigh()            do { LATCbits.LATC1 = 1; } while(0)
#define KEY_10_SetLow()             do { LATCbits.LATC1 = 0; } while(0)
#define KEY_10_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define KEY_10_GetValue()           PORTCbits.RC1
#define KEY_10_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define KEY_10_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)
#define KEY_10_SetPullup()          do { WPUCbits.WPUC1 = 1; } while(0)
#define KEY_10_ResetPullup()        do { WPUCbits.WPUC1 = 0; } while(0)
#define KEY_10_SetPushPull()        do { ODCONCbits.ODCC1 = 0; } while(0)
#define KEY_10_SetOpenDrain()       do { ODCONCbits.ODCC1 = 1; } while(0)
#define KEY_10_SetAnalogMode()      do { ANSELCbits.ANSELC1 = 1; } while(0)
#define KEY_10_SetDigitalMode()     do { ANSELCbits.ANSELC1 = 0; } while(0)

// get/set KEY_11 aliases
#define KEY_11_TRIS                 TRISCbits.TRISC2
#define KEY_11_LAT                  LATCbits.LATC2
#define KEY_11_PORT                 PORTCbits.RC2
#define KEY_11_WPU                  WPUCbits.WPUC2
#define KEY_11_OD                   ODCONCbits.ODCC2
#define KEY_11_ANS                  ANSELCbits.ANSELC2
#define KEY_11_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define KEY_11_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define KEY_11_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define KEY_11_GetValue()           PORTCbits.RC2
#define KEY_11_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define KEY_11_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)
#define KEY_11_SetPullup()          do { WPUCbits.WPUC2 = 1; } while(0)
#define KEY_11_ResetPullup()        do { WPUCbits.WPUC2 = 0; } while(0)
#define KEY_11_SetPushPull()        do { ODCONCbits.ODCC2 = 0; } while(0)
#define KEY_11_SetOpenDrain()       do { ODCONCbits.ODCC2 = 1; } while(0)
#define KEY_11_SetAnalogMode()      do { ANSELCbits.ANSELC2 = 1; } while(0)
#define KEY_11_SetDigitalMode()     do { ANSELCbits.ANSELC2 = 0; } while(0)

// get/set LED aliases
#define LED_TRIS                 TRISCbits.TRISC3
#define LED_LAT                  LATCbits.LATC3
#define LED_PORT                 PORTCbits.RC3
#define LED_WPU                  WPUCbits.WPUC3
#define LED_OD                   ODCONCbits.ODCC3
#define LED_ANS                  ANSELCbits.ANSELC3
#define LED_SetHigh()            do { LATCbits.LATC3 = 1; } while(0)
#define LED_SetLow()             do { LATCbits.LATC3 = 0; } while(0)
#define LED_Toggle()             do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define LED_GetValue()           PORTCbits.RC3
#define LED_SetDigitalInput()    do { TRISCbits.TRISC3 = 1; } while(0)
#define LED_SetDigitalOutput()   do { TRISCbits.TRISC3 = 0; } while(0)
#define LED_SetPullup()          do { WPUCbits.WPUC3 = 1; } while(0)
#define LED_ResetPullup()        do { WPUCbits.WPUC3 = 0; } while(0)
#define LED_SetPushPull()        do { ODCONCbits.ODCC3 = 0; } while(0)
#define LED_SetOpenDrain()       do { ODCONCbits.ODCC3 = 1; } while(0)
#define LED_SetAnalogMode()      do { ANSELCbits.ANSELC3 = 1; } while(0)
#define LED_SetDigitalMode()     do { ANSELCbits.ANSELC3 = 0; } while(0)

// get/set ENC0_A aliases
#define ENC0_A_TRIS                 TRISCbits.TRISC4
#define ENC0_A_LAT                  LATCbits.LATC4
#define ENC0_A_PORT                 PORTCbits.RC4
#define ENC0_A_WPU                  WPUCbits.WPUC4
#define ENC0_A_OD                   ODCONCbits.ODCC4
#define ENC0_A_ANS                  ANSELCbits.ANSELC4
#define ENC0_A_SetHigh()            do { LATCbits.LATC4 = 1; } while(0)
#define ENC0_A_SetLow()             do { LATCbits.LATC4 = 0; } while(0)
#define ENC0_A_Toggle()             do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define ENC0_A_GetValue()           PORTCbits.RC4
#define ENC0_A_SetDigitalInput()    do { TRISCbits.TRISC4 = 1; } while(0)
#define ENC0_A_SetDigitalOutput()   do { TRISCbits.TRISC4 = 0; } while(0)
#define ENC0_A_SetPullup()          do { WPUCbits.WPUC4 = 1; } while(0)
#define ENC0_A_ResetPullup()        do { WPUCbits.WPUC4 = 0; } while(0)
#define ENC0_A_SetPushPull()        do { ODCONCbits.ODCC4 = 0; } while(0)
#define ENC0_A_SetOpenDrain()       do { ODCONCbits.ODCC4 = 1; } while(0)
#define ENC0_A_SetAnalogMode()      do { ANSELCbits.ANSELC4 = 1; } while(0)
#define ENC0_A_SetDigitalMode()     do { ANSELCbits.ANSELC4 = 0; } while(0)

// get/set ENC0_B aliases
#define ENC0_B_TRIS                 TRISCbits.TRISC5
#define ENC0_B_LAT                  LATCbits.LATC5
#define ENC0_B_PORT                 PORTCbits.RC5
#define ENC0_B_WPU                  WPUCbits.WPUC5
#define ENC0_B_OD                   ODCONCbits.ODCC5
#define ENC0_B_ANS                  ANSELCbits.ANSELC5
#define ENC0_B_SetHigh()            do { LATCbits.LATC5 = 1; } while(0)
#define ENC0_B_SetLow()             do { LATCbits.LATC5 = 0; } while(0)
#define ENC0_B_Toggle()             do { LATCbits.LATC5 = ~LATCbits.LATC5; } while(0)
#define ENC0_B_GetValue()           PORTCbits.RC5
#define ENC0_B_SetDigitalInput()    do { TRISCbits.TRISC5 = 1; } while(0)
#define ENC0_B_SetDigitalOutput()   do { TRISCbits.TRISC5 = 0; } while(0)
#define ENC0_B_SetPullup()          do { WPUCbits.WPUC5 = 1; } while(0)
#define ENC0_B_ResetPullup()        do { WPUCbits.WPUC5 = 0; } while(0)
#define ENC0_B_SetPushPull()        do { ODCONCbits.ODCC5 = 0; } while(0)
#define ENC0_B_SetOpenDrain()       do { ODCONCbits.ODCC5 = 1; } while(0)
#define ENC0_B_SetAnalogMode()      do { ANSELCbits.ANSELC5 = 1; } while(0)
#define ENC0_B_SetDigitalMode()     do { ANSELCbits.ANSELC5 = 0; } while(0)

// get/set ENC0_S aliases
#define ENC0_S_TRIS                 TRISCbits.TRISC6
#define ENC0_S_LAT                  LATCbits.LATC6
#define ENC0_S_PORT                 PORTCbits.RC6
#define ENC0_S_WPU                  WPUCbits.WPUC6
#define ENC0_S_OD                   ODCONCbits.ODCC6
#define ENC0_S_ANS                  ANSELCbits.ANSELC6
#define ENC0_S_SetHigh()            do { LATCbits.LATC6 = 1; } while(0)
#define ENC0_S_SetLow()             do { LATCbits.LATC6 = 0; } while(0)
#define ENC0_S_Toggle()             do { LATCbits.LATC6 = ~LATCbits.LATC6; } while(0)
#define ENC0_S_GetValue()           PORTCbits.RC6
#define ENC0_S_SetDigitalInput()    do { TRISCbits.TRISC6 = 1; } while(0)
#define ENC0_S_SetDigitalOutput()   do { TRISCbits.TRISC6 = 0; } while(0)
#define ENC0_S_SetPullup()          do { WPUCbits.WPUC6 = 1; } while(0)
#define ENC0_S_ResetPullup()        do { WPUCbits.WPUC6 = 0; } while(0)
#define ENC0_S_SetPushPull()        do { ODCONCbits.ODCC6 = 0; } while(0)
#define ENC0_S_SetOpenDrain()       do { ODCONCbits.ODCC6 = 1; } while(0)
#define ENC0_S_SetAnalogMode()      do { ANSELCbits.ANSELC6 = 1; } while(0)
#define ENC0_S_SetDigitalMode()     do { ANSELCbits.ANSELC6 = 0; } while(0)

// get/set ENC1_A aliases
#define ENC1_A_TRIS                 TRISCbits.TRISC7
#define ENC1_A_LAT                  LATCbits.LATC7
#define ENC1_A_PORT                 PORTCbits.RC7
#define ENC1_A_WPU                  WPUCbits.WPUC7
#define ENC1_A_OD                   ODCONCbits.ODCC7
#define ENC1_A_ANS                  ANSELCbits.ANSELC7
#define ENC1_A_SetHigh()            do { LATCbits.LATC7 = 1; } while(0)
#define ENC1_A_SetLow()             do { LATCbits.LATC7 = 0; } while(0)
#define ENC1_A_Toggle()             do { LATCbits.LATC7 = ~LATCbits.LATC7; } while(0)
#define ENC1_A_GetValue()           PORTCbits.RC7
#define ENC1_A_SetDigitalInput()    do { TRISCbits.TRISC7 = 1; } while(0)
#define ENC1_A_SetDigitalOutput()   do { TRISCbits.TRISC7 = 0; } while(0)
#define ENC1_A_SetPullup()          do { WPUCbits.WPUC7 = 1; } while(0)
#define ENC1_A_ResetPullup()        do { WPUCbits.WPUC7 = 0; } while(0)
#define ENC1_A_SetPushPull()        do { ODCONCbits.ODCC7 = 0; } while(0)
#define ENC1_A_SetOpenDrain()       do { ODCONCbits.ODCC7 = 1; } while(0)
#define ENC1_A_SetAnalogMode()      do { ANSELCbits.ANSELC7 = 1; } while(0)
#define ENC1_A_SetDigitalMode()     do { ANSELCbits.ANSELC7 = 0; } while(0)

// get/set KEY_00 aliases
#define KEY_00_PORT                 PORTEbits.RE3
#define KEY_00_WPU                  WPUEbits.WPUE3
#define KEY_00_GetValue()           PORTEbits.RE3
#define KEY_00_SetPullup()          do { WPUEbits.WPUE3 = 1; } while(0)
#define KEY_00_ResetPullup()        do { WPUEbits.WPUE3 = 0; } while(0)

void PIN_MANAGER_Initialize (void);

#endif