#ifndef EEPROM_H
#define	EEPROM_H
#include <xc.h>

void EEPROM_Write(uint16_t address, uint8_t data);
uint8_t EEPROM_Read(uint16_t address);

#endif

