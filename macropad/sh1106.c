#include "sh1106.h"
#include "eeprom.h"

static uint8_t buffer[128*64/8] = {};

void SPI_Initialize(void)
{
    // SPI registers setting
    SPI1CON0 = 0b00000011;
    SPI1CON1 = 0b00110100;
    SPI1CON2 = 0b00000010;
    SPI1CLK =  0b00000001;
    SPI1BAUD = 0b00001111;
    
    // enable SPI
    SPI1CON0bits.EN = 1;
}

void SH1106_WriteCommand(uint8_t command)
{
    SH1106_DC_SetLow();    
    SPI1TXB = command; 
    while(!SPI1STATUSbits.TXBE); // wait until TxFIFO is empty
}

void SH1106_WriteData(uint8_t data)
{
    SH1106_DC_SetHigh();    
    SPI1TXB = data; 
    while(!SPI1STATUSbits.TXBE); // wait until TxFIFO is empty
}

void SH1106_Initialize(void)
{
    __delay_ms(1);
    SH1106_RES_SetHigh();
    
    // rotates the display 180 degrees
    SH1106_WriteCommand(CMD_FLIP_HOR);  
    SH1106_WriteCommand(CMD_FLIP_VERT);
    
    SH1106_SetContrast(128);
    SH1106_ClearBuffer();
    SH1106_DisplayBuffer();    
    SH1106_WriteCommand(CMD_DISPLAY_ON);
    __delay_ms(100);
}

void SH1106_SetContrast(uint8_t contrast)
{
    // set contrast 0-255
    SH1106_WriteCommand(CMD_SET_CONTRAST);
    SH1106_WriteCommand(contrast);
}

void SH1106_Goto(uint8_t page, uint8_t column)
{
    // set page 0-7
    SH1106_WriteCommand(0b10110000 + (0b00000111 & page));
    
    // set column 0-127
    // the column adress is increased by 2 - SH1106 supports 132 columns but the display has only 128
    SH1106_WriteCommand(0b00010000 + (0b00001111 & (column>>4)));   // higher bits
    SH1106_WriteCommand(0b00000000 + (0b00001111 & (column + 2)));  // lower bits
}

void SH1106_ClearBuffer(void)
{
    memset(buffer, 0, sizeof(buffer));
}

void SH1106_DisplayBuffer(void)
{
    for(uint8_t page = 0; page < 8; page++)
    {
        SH1106_Goto(page, 0);
        for(uint8_t column = 0; column < 128; column++)
        {
            SH1106_WriteData(buffer[page * 128 + column]);
        }
    }
    SH1106_Goto(0, 0);
}

void SH1106_DrawPixel(uint8_t x, uint8_t y, uint8_t color)
{
    // color: BLACK, WHITE, INVERSE
    // left upper corner is (0,0)
    
    if((x > 127) || (y > 63))
        return;
    
    switch(color)
    {
        case BLACK: buffer[(y/8)*128 + x] &= ~(1 << y%8); break;
        case WHITE: buffer[(y/8)*128 + x] |= (1 << y%8); break;
        case INVERSE: buffer[(y/8)*128 + x] ^= (1 << y%8); break;   
    }
    
}

void SH1106_DrawRectangle(uint8_t x, uint8_t y, uint8_t w, uint8_t h)
{
    // x, y - location of the left upper corner
    // w, h - width and height of the rectangle
    
    if((x + w > 128) || (y + h > 64))
        return;
    
    for(uint8_t i = 0; i < w; i++)
    {
        SH1106_DrawPixel(x + i, y, WHITE);
        SH1106_DrawPixel(x + i, y + h - 1, WHITE);        
    }
    
    for(uint8_t i = 1; i < h-1; i++)
    {
        SH1106_DrawPixel(x, y + i, WHITE);
        SH1106_DrawPixel(x + w - 1, y + i, WHITE);        
    }   
}

void SH1106_DrawChar(uint8_t x, uint8_t y, char c, uint8_t color)
{
    // color: BLACK, WHITE
    // x, y - location of the left upper corner
    
    if((x > 127 - 6) || (y > 63 - 8) || (color != 0 && color != 1))
        return;
    
    uint16_t index = 6 * (c - ' ');
    
    for(uint8_t w = 0; w < FONT_WIDTH; w++)
    {
        for(uint8_t h = 0; h < FONT_HEIGHT; h++)
        {
            // loops through individual bits in the byte
            if(((font[index + w] >> h) & 1) == 1)
                SH1106_DrawPixel(x + w, y + h, color);
        }
    }
}

void SH1106_DrawText(uint8_t x, uint8_t y, char text[], uint8_t length, uint8_t color)
{
    // color: BLACK, WHITE
    
    //if(strlen(text) < length)
    //    length = (uint8_t)strlen(text);
    
    for(uint8_t i = 0; i < length; i++)
    {
        SH1106_DrawChar(x + 6*i, y, text[i], color);
    }
}


void SH1106_DisplayProfile(uint8_t profile)
{
    SH1106_ClearBuffer();
    SH1106_DrawRectangle(0, 0, 128, 64);

    // draw vertical lines
    for(uint8_t h = 0; h < 48; h++)
    {
        SH1106_DrawPixel(42, 16 + h, WHITE);
    }    
    for(uint8_t h = 0; h < 48; h++)
    {
        SH1106_DrawPixel(85, 16 + h, WHITE);
    }

    // draw horizontal lines
    for(uint8_t i = 0; i < 4; i++)
    {
        for(uint8_t w = 0; w < 126; w++)
        {
            SH1106_DrawPixel(1 + w, 63 - (i + 1)*12, WHITE);
        }
    } 
    
    // draw profile name background
    for(uint8_t x = 1; x < 127; x++)
    {
        for(uint8_t y = 1; y < 15; y++)
        {
            SH1106_DrawPixel(x, y, WHITE);
        }
    }
    
    // draw text        
    for(uint8_t x = 0; x < 3; x++)
    {
        for(uint8_t y = 0; y < 4; y++)
        {
            if(x != 2)
                SH1106_DrawText(2 + 42*x, 18 + 12*y, profiles[profile].key_names[x + 3*y], 6, WHITE);
            else
                SH1106_DrawText(3 + 42*x, 18 + 12*y, profiles[profile].key_names[x + 3*y], 6, WHITE);
        }
    } 
    SH1106_DrawText(2, 4, profiles[profile].profile_name, 12, BLACK);
    
    SH1106_DisplayBuffer();
}

void SH1106_InvertKey(uint8_t key_number)
{
    // key_number: 0 - 11
    // key0: row = 0, col = 0
    
    uint8_t key_row = key_number / 3;
    uint8_t key_col = key_number % 3;    
    
    if(key_col == 0)
    {
        for(uint8_t x = 0; x < 41; x++)
        {
            for(uint8_t y = 0; y < 11; y++)
            {
                SH1106_DrawPixel(1 + x, 16 + 12*key_row + y, INVERSE);
            }
        }        
    }    
    else if(key_col == 1)
    {
        for(uint8_t x = 0; x < 42; x++)
        {
            for(uint8_t y = 0; y < 11; y++)
            {
                SH1106_DrawPixel(43 + x, 16 + 12*key_row + y, INVERSE);
            }
        }        
    }
    else
    {
        for(uint8_t x = 0; x < 41; x++)
        {
            for(uint8_t y = 0; y < 11; y++)
            {
                SH1106_DrawPixel(86 + x, 16 + 12*key_row + y, INVERSE);
            }
        }        
    }
    
    SH1106_DisplayBuffer();        
}