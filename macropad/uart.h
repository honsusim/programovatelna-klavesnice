#ifndef UART_H
#define	UART_H
#include <xc.h>
#include <string.h>

void UART_Initialize(void);
void UART_Write(uint8_t data);
uint8_t UART_Read(void);
void UART_WriteString(char string[]);

#endif

