#include <stdio.h>
#include "device_config.h"
#include "pin_manager.h"
#include "uart.h"
#include "sh1106.h"
#include "eeprom.h"
#include "led.h"
#include "profile.h"

void ENC_Scan(void)
{
    static int A0_prev = 1;
    int A0 = ENC0_A_PORT;
    int B0 = ENC0_B_PORT;
    
    // scan ENC0 rotation
    if(A0 != A0_prev) // A0 changed
    {
        if(B0 == 0)
        {
            if(A0_prev == 0) // CW
                UART_WriteString("E0+");
            else // CCW
                UART_WriteString("E0-");
        }
        A0_prev = A0; // update a status
    }
    
    // scan ENC0 switch
    if(ENC0_S_PORT == 0)
    {
        __delay_ms(20);             // press debounce
        UART_WriteString("E0S");
        PROFILE_Change(PREV);
        while(ENC0_S_PORT == 0);    // wait while the key is pressed
        __delay_ms(20);             // release debounce    
    }
        
    static int A1_prev = 1;
    int A1 = ENC1_A_PORT;
    int B1 = ENC1_B_PORT;
    
    // scan ENC1 rotation
    if(A1 != A1_prev) // A1 changed
    {
        if(B1 == 0)
        {
            if(A1_prev == 0) // CW
                UART_WriteString("E1+");
            else // CCW
                UART_WriteString("E1-");
        }
        A1_prev = A1; // update a status
    }
    
    // scan ENC1 switch
    if(ENC1_S_PORT == 0)
    {
        __delay_ms(20);             // press debounce
        UART_WriteString("E1S"); 
        PROFILE_Change(NEXT);
        while(ENC1_S_PORT == 0);    // wait while the key is pressed
        __delay_ms(20);             // release debounce      
    }
}

void KEY_Scan(void)
{
    // load key status
    uint8_t keys[12] = {KEY_00_PORT, KEY_01_PORT, KEY_02_PORT,
                        KEY_03_PORT, KEY_04_PORT, KEY_05_PORT,
                        KEY_06_PORT, KEY_07_PORT, KEY_08_PORT,
                        KEY_09_PORT, KEY_10_PORT, KEY_11_PORT};
    
    for(uint8_t i = 0; i < 12; i++)
    {
        if(!keys[i])
        {
            __delay_ms(20); // press debounce
            SH1106_InvertKey(i);
            
            // create and send UART message K00 - K11
            char msg[3];
            snprintf(msg, 4, "K%0*i", 2, i);
            UART_WriteString(msg);
            
            // wait while any key is pressed
            while(!KEY_00_PORT || !KEY_01_PORT || !KEY_02_PORT ||
                  !KEY_03_PORT || !KEY_04_PORT || !KEY_05_PORT ||
                  !KEY_06_PORT || !KEY_07_PORT || !KEY_08_PORT ||
                  !KEY_09_PORT || !KEY_10_PORT || !KEY_11_PORT);  
            
            SH1106_InvertKey(i);
            __delay_ms(20); // release debounce
        }
    }
}

void main(void)
{
    SYSTEM_Initialize();
    
    while(1)
    {   
        KEY_Scan();
        ENC_Scan();
        
        // receive configuration
        if(PIR3bits.U1RXIF)
        {
            uint8_t active = UART_Read() - '0'; // load active profile (should be 0)
            if(active != 0)
                return;    

            EEPROM_Write(0, active);            // save active profile
            uint8_t count = UART_Read() - '0';  // load profile count
            EEPROM_Write(1, count);             // save profile count    

            // save individual profiles
            for(uint16_t i = 0; i < (count + 1)*84; i++)
            {
                char data = UART_Read();
                EEPROM_Write(i + 2, data);          
            } 
            
            LED_SetAll(0, 50, 0); // blink green
            __delay_ms(200);
            asm("RESET");
        }
    }
}

