#include <xc.h>
#include "uart.h"

void UART_Initialize(void)
{
    // P1L 0
    U1P1L = 0x00;
    // P1H 0
    U1P1H = 0x00;
    // P2L 0 
    U1P2L = 0x00;
    // P2H 0 
    U1P2H = 0x00;
    // P3L 0 
    U1P3L = 0x00;
    // P3H 0 
    U1P3H = 0x00;
    // BRGS high speed; MODE Asynchronous 8-bit mode; RXEN enabled; TXEN enabled; ABDEN disabled 
    U1CON0 = 0xB0;
    // RXBIMD Set RXBKIF on rising RX input; BRKOVR disabled; WUE disabled; SENDB disabled; ON enabled 
    U1CON1 = 0x80;
    // TXPOL not inverted; FLO off; C0EN Checksum Mode 0; RXPOL not inverted; RUNOVF RX input shifter stops all activity; STP Transmit 1Stop bit, receiver verifies first Stop bit 
    U1CON2 = 0x00;
    // BRGL 138 -> baud rate = fosc/[4*(BRG + 1)] = 115200  
    U1BRGL = 0x8A;
    // BRGH 0 
    U1BRGH = 0x00;
    // STPMD in middle of first Stop bit; TXWRE No error 
    U1FIFO = 0x00;
    // ABDIF Auto-baud not enabled or not complete; WUIF WUE not enabled by software; ABDIE disabled 
    U1UIR = 0x00;
    // ABDOVF Not overflowed; TXCIF 0; RXBKIF No Break detected; RXFOIF not overflowed; CERIF No Checksum error 
    U1ERRIR = 0x00;
    // TXCIE disabled; FERIE disabled; TXMTIE disabled; ABDOVE disabled; CERIE disabled; RXFOIE disabled; PERIE disabled; RXBKIE disabled 
    U1ERRIE = 0x00;
    
    // enable UART receive interrupt
    //INTCON0bits.IPEN = 0;   // disable interrupt priority
    //PIE3bits.U1RXIE = 1;    // enable UART1 receive interrupt  
    //INTCON0bits.GIE = 1;    // enable global interrupts    
}

void UART_Write(uint8_t data)
{
    while(!PIR3bits.U1TXIF);    // wait while transmitter is busy
    U1TXB = data;               // write data into UART1 transmit register
}

uint8_t UART_Read(void)
{
    while(!PIR3bits.U1RXIF);    // wait while the reciever is busy   
    return U1RXB;               // return recieved data         
}

void UART_WriteString(char string[])
{
    for(uint8_t i = 0; i < strlen(string); i++)
    {
        UART_Write(string[i]);
    }
}