#ifndef PROFILES_H
#define	PROFILES_H
#include <xc.h>
#include "uart.h"
#include "eeprom.h"
#include "sh1106.h"

#define PREV 0
#define NEXT 1

// profile structure declaration
typedef struct
{
    char profile_name[12];
    char key_names[12][6];
} profile;

uint8_t profile_active; // 1 byte
uint8_t profile_count;  // 1 byte (0 - 9)
profile profiles[10];   // 840 bytes

void PROFILE_Initialize(void);
void PROFILE_Change(uint8_t dir);

#endif

