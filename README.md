# Programovatelná klávesnice

Pro většinu z nás je používání počítače každodenní záležitostí. Za účelem zefektivnění práce na počítači jsem vytvořil tuto programovatelnou klávesnici neboli macropad, která uživateli poskytuje jednoduchý přístup k často používaným funkcím a klávesovým zkratkám.

Demonstrační video: https://youtu.be/otlEJqZ3GW4

### Hlavní komponenty macropadu
- 12 programovatelných kláves
    - využívají mechanické spínače
    - hot-swappable sockety pro jednoduchou výměnu spínačů
    - individuální podsvícení kláves pomocí RGB LED
- 1,3" OLED displej s rozlišením 128 x 64 pixelů
    - zobrazuje aktivní profil
    - indikuje stisk tlačítka
- 2 rotační enkodéry
- USB C konektor

<img src="/images/macropad.jpg" width="60%" />

Macropad je řízen 8bitovým mikrokontrolérem řady PIC18 a pro komunikaci s počítačem je použit UART-USB převodník FT232RL. Díky němu je na počítači klávesnice rozpoznána jako virtuální sériový port, který umožňuje jednoduchý obousměrný přenos dat.

Pomocí doprovodné počítačové aplikace lze jednoduše vytvořit až 10 profilů, které specifikují funkce kláves a enkodérů macropadu. Klávesám lze také přiřadit popisky, které jsou společně s názvem příslušného profilu po nahrání konfigurace do klávesnice zobrazovány na displeji. Mezi profily lze pak rychle přepínat stisky enkodérů.

<p align="middle">
  <img src="/images/pcb_front.jpg" height="400" />
  <img src="/images/pcb_back.jpg" height="400" />
</p>

## Obsah repozitáře
- *app* - počítačová aplikace a její zdrojový kód
- *bootloader* - zdrojový kód programového zavaděče mikrokontroléru
- *images* - obrázky
- *kicad* - KiCad projekt s návrhem zapojení a DPS
- *macropad* - zdrojový kód cílového programu mikrokontroléru

## Počítačová aplikace
Doprovodná počítačová aplikace *Macropad configuration* pro Windows byla vytvořena pomocí programovacího jazyka Python a frameworku Qt. 

Aplikace slouží ke konfiguraci klávesnice, ale i ke zpracovávání příchozích dat a vykonávání požadovaných funkcí. Proto je pro funkci klávesnice nutné, aby byl software na počítači neustále spuštěn. Z toho důvodu je aplikace spouštěna na pozadí a lze také zapnout její automatické spuštění s počítačem.

<img src="/images/gui.png" width="90%" />

### Rozhraní aplikace
V horní části okna aplikace se nachází nástrojová lišta s tlačítky pro přidání/odebrání/přejmenování profilu. Dále jsou v ní tlačítka pro uložení/nahrání konfigurace a je v ní také zobrazen stav klávesnice a sériový port, na který je připojena.

Převážná část okna aplikace je tvořena záložkami, které reprezentují jednotlivé profily. Každý profil má svůj název a uvnitř jeho záložky jsou přehledně zobrazena nastavení jednotlivých kláves a enkodérů v souladu s jejich fyzickým rozložením.

### Funkce kláves
- ovládání médií
    - pauza
    - následující skladba
    - předchozí skladba
    - zlumení
    - snížení/zvýšení hlasitosti
- vyvolání nahrané klávesové zkratky
- výpis nastaveného textu
- spuštění uživatelského Python skriptu

### Funkce enkodérů
- snížení/zvýšení hlasitosti
- vertikální scrollování
- horizontální scrollování

### Detekce klávesnice
Důležitou funkcí aplikace je také automatická detekce připojení klávesnice. Pokud klávesnice není k počítači připojena, tak aplikace periodicky skenuje všechny aktivní sériové porty a kontroluje VID, PID a sériové číslo připojených zařízení. Pokud tyto hodnoty některého z portů odpovídají použitému čipu FT232, tak se aplikace na příslušný sériový port připojí a čeká na data z klávesnice.

### Nahrávání konfigurace
Po stisku tlačítka pro nahrávání konfigurace jsou do macropadu odeslány názvy profilů a popisky kláves. Veškerá tato data jsou uložena do EEPROM paměti mikrokontroléru a po jeho restartu jsou načtena a zobrazena na displeji.
